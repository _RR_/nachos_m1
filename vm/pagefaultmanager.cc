/*! \file pagefaultmanager.cc
  Routines for the page fault managerPage Fault Manager
*/
//
//  Copyright (c) 1999-2000 INSA de Rennes.
//  All rights reserved.  
//  See copyright_insa.h for copyright notice and limitation 
//  of liability and disclaimer of warranty provisions.
//

#include "kernel/thread.h"
#include "vm/swapManager.h"
#include "vm/physMem.h"
#include "vm/pagefaultmanager.h"

PageFaultManager::PageFaultManager() {
}

// PageFaultManager::~PageFaultManager()
/*! Nothing for now
 */
PageFaultManager::~PageFaultManager() {
}

// ExceptionType PageFault(uint32_t virtualPage)
/*! 	
//	This method is called by the Memory Management Unit when there is a 
//      page fault. This method loads the page from :
//      - read-only sections (text,rodata) $\Rightarrow$ executive
//        file
//      - read/write sections (data,...) $\Rightarrow$ executive
//        file (1st time only), or swap file
//      - anonymous mappings (stack/bss) $\Rightarrow$ new
//        page from the MemoryManager (1st time only), or swap file
//
//	\param virtualPage the virtual page subject to the page fault
//	  (supposed to be between 0 and the
//        size of the address space, and supposed to correspond to a
//        page mapped to something [code/data/bss/...])
//	\return the exception (generally the NO_EXCEPTION constant)
*/  
ExceptionType PageFaultManager::PageFault(uint32_t virtualPage) 
{

#ifndef ETUDIANTS_TP
  printf("**** Warning: page fault manager is not implemented yet\n");
  exit(-1);
  return ((ExceptionType)0);

#else

  while(g_machine->mmu->translationTable->getBitIo(virtualPage)){
    g_current_thread->Yield();
  }
  if(!g_machine->mmu->translationTable->getBitValid(virtualPage)){
    AddrSpace* addrspace = g_current_thread->GetProcessOwner()->addrspace;
    int pp = g_physical_mem_manager->AddPhysicalToVirtualMapping(addrspace, virtualPage);
    int addr = g_machine->mmu->translationTable->getAddrDisk(virtualPage);
    
    if(g_machine->mmu->translationTable->getBitSwap(virtualPage)){
      // load from the swap
      g_swap_manager->GetPageSwap(addr, (char*) &g_machine->mainMemory[g_cfg->PageSize*pp]);   
    }
    else {
      if(addr > 0){
	// load from the disk
      
	OpenFile* exec_file = g_current_thread->GetProcessOwner()->exec_file;
	exec_file->ReadAt((char*) &g_machine->mainMemory[g_cfg->PageSize*pp],
			  g_cfg->PageSize,
			  addr);
      }
      else{
	
      }
    }
  }
  // else the page have already been mapped so we have nothing to do
  return NO_EXCEPTION;

#endif
}




