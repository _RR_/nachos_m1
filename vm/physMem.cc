//-----------------------------------------------------------------
/*! \file mem.cc
//  \brief Routines for the physical page management
*/
//
//  Copyright (c) 1999-2000 INSA de Rennes.
//  All rights reserved.  
//  See copyright_insa.h for copyright notice and limitation 
//  of liability and disclaimer of warranty provisions.
//-----------------------------------------------------------------

#include <unistd.h>
#include "vm/physMem.h"

//-----------------------------------------------------------------
// PhysicalMemManager::PhysicalMemManager
//
/*! Constructor. It simply clears all the page flags and inserts them in the
// free_page_list to indicate that the physical pages are free
*/
//-----------------------------------------------------------------
PhysicalMemManager::PhysicalMemManager() {

  long i;

  tpr = new struct tpr_c[g_cfg->NumPhysPages];

  for (i=0;i<g_cfg->NumPhysPages;i++) {
    tpr[i].free=true;
    tpr[i].locked=false;
    tpr[i].owner=NULL;
    free_page_list.Append((void*)i);
  }
  i_clock=-1;
}

PhysicalMemManager::~PhysicalMemManager() {
  // Empty free page list
  int64_t page;
  while (!free_page_list.IsEmpty()) page =  (int64_t)free_page_list.Remove();

  // Delete physical page table
  delete[] tpr;
}

//-----------------------------------------------------------------
// PhysicalMemManager::RemovePhysicalToVitualMapping
//
/*! This method releases an unused physical page by clearing the
//  corresponding bit in the page_flags bitmap structure, and adding
//  it in the free_page_list.
//
//  \param num_page is the number of the real page to free
*/
//-----------------------------------------------------------------
void PhysicalMemManager::RemovePhysicalToVirtualMapping(long num_page) {

  // Check that the page is not already free 
  ASSERT(!tpr[num_page].free);

  // Update the physical page table entry
  tpr[num_page].free=true;
  tpr[num_page].locked=false;
  if (tpr[num_page].owner->translationTable!=NULL) 
    tpr[num_page].owner->translationTable->clearBitValid(tpr[num_page].virtualPage);

  // Insert the page in the free list
  free_page_list.Prepend((void*)num_page);
}

//-----------------------------------------------------------------
// PhysicalMemManager::UnlockPage
//
/*! This method unlocks the page numPage, after
//  checking the page is in the locked state. Used
//  by the page fault manager to unlock at the
//  end of a page fault (the page cannot be evicted until
//  the page fault handler terminates).
//
//  \param num_page is the number of the real page to unlock
*/
//-----------------------------------------------------------------
void PhysicalMemManager::UnlockPage(long num_page) {
  ASSERT(num_page<g_cfg->NumPhysPages);
  ASSERT(tpr[num_page].locked==true);
  ASSERT(tpr[num_page].free==false);
  tpr[num_page].locked = false;
}

//-----------------------------------------------------------------
// PhysicalMemManager::ChangeOwner
//
/*! Change the owner of a page
//
//  \param owner is a pointer on new owner (Thread *)
//  \param numPage is the concerned page
*/
//-----------------------------------------------------------------
void PhysicalMemManager::ChangeOwner(long numPage, Thread* owner) {
  // Update statistics
  g_current_thread->GetProcessOwner()->stat->incrMemoryAccess();
  // Change the page owner
  tpr[numPage].owner = owner->GetProcessOwner()->addrspace;
}

//-----------------------------------------------------------------
// PhysicalMemManager::AddPhysicalToVirtualMapping 
//
/*! This method returns a new physical page number. If there is no
//  page available, it evicts one page (page replacement algorithm).
//
//  NB: this method locks the newly allocated physical page such that
//      it is not stolen during the page fault resolution. Don't forget
//      to unlock it
//
//  \param owner address space (for backlink)
//  \param virtualPage is the number of virtualPage to link with physical page
//  \return A new physical page number.
*/
//-----------------------------------------------------------------
int PhysicalMemManager::AddPhysicalToVirtualMapping(AddrSpace* owner,int virtualPage) 
{

#ifndef ETUDIANTS_TP
  printf("**** Warning: function AddPhysicalToVirtualMapping is not implemented\n");
  exit(-1);
  return (0);
#else

  int pp = FindFreePage();
  if(pp < 0)
    {
      pp = EvictPage();
    } 
  
  tpr[pp].free=false;
  tpr[pp].locked=true;
  tpr[pp].owner = owner;

  owner->translationTable->setPhysicalPage(virtualPage,pp);
  owner->translationTable->setBitValid(virtualPage);
  return pp;
#endif
}

//-----------------------------------------------------------------
// PhysicalMemManager::FindFreePage
//
/*! This method returns a new physical page number, if it finds one
//  free. If not, return -1. Does not run the clock algorithm.
//
//  \return A new free physical page number.
*/
//-----------------------------------------------------------------
int PhysicalMemManager::FindFreePage() {
  int64_t page;

  // Check that the free list is not empty
  if (free_page_list.IsEmpty())
    return -1;

  // Update statistics
  g_current_thread->GetProcessOwner()->stat->incrMemoryAccess();

  // Get a page from the free list
  page = (int64_t)free_page_list.Remove();

  // Check that the page is really free
  ASSERT(tpr[page].free);

  // Update the physical page table
  tpr[page].free = false;

  return page;
}

//-----------------------------------------------------------------
// PhysicalMemManager::EvictPage
//
/*! This method implements page replacement, using the well-known
//  clock algorithm.
//
//  \return A new free physical page number.
*/
//-----------------------------------------------------------------
int PhysicalMemManager::EvictPage() {

  int old_i_clock = i_clock;
  int all_locked = 1;
  int u_bit = 1;
  int first_loop = 1;

  while(1) {
    // keep bit on wether all pages are locked
    all_locked = tpr[i_clock].locked && all_locked;
    // if after a clock turn all pages are locked
    if(all_locked && old_i_clock == i_clock && !first_loop) {
      // go run other things
      g_current_thread->Yield();
    }

    u_bit = (tpr[i_clock].owner!=NULL) ? tpr[i_clock].owner->translationTable->getBitU(tpr[i_clock].virtualPage) : 0;
    if(u_bit == 0 && !tpr[i_clock].locked) {
      // We got our page 
      break;
    }
    else {
      // Or not
      // set used bit to 0 for LRU
      if(tpr[i_clock].owner!=NULL) { tpr[i_clock].owner->translationTable->setBitU(0); }
    }

    i_clock = (i_clock + 1) % g_cfg->NumPhysPages;
    first_loop = 0;
  }
  tpr[i_clock].locked = true;
  // if owner exist if not no point
  if(tpr[i_clock].owner != NULL) {
    // get modified bit
    int m_bit = tpr[i_clock].owner->translationTable->getBitM(tpr[i_clock].virtualPage);
    if(m_bit) {
      // swap memory if needed
      if(tpr[i_clock].owner->translationTable->getBitM(tpr[i_clock].virtualPage)) {
        int swap_place = g_swap_manager->PutPageSwap(-1, (char*)&g_machine->mainMemory[g_cfg->PageSize * i_clock]);
        // set wap bit
        tpr[i_clock].owner->translationTable->setBitSwap(tpr[i_clock].virtualPage);
        tpr[i_clock].owner->translationTable->setAddrDisk(tpr[i_clock].virtualPage, swap_place);
        tpr[i_clock].owner->translationTable->clearBitM(tpr[i_clock].virtualPage);
      }
    }
    // remove current mapping
    tpr[i_clock].owner->translationTable->clearBitValid(tpr[i_clock].virtualPage);
  }
  tpr[i_clock].free = false;
  tpr[i_clock].locked = false;
  /*
  printf("**** Warning: page replacement algorithm is not implemented yet\n");
  exit(-1);
  */
  return i_clock;
}

//-----------------------------------------------------------------
// PhysicalMemManager::Print
//
/*! print the current status of the table of physical pages
//
//  \param rpage number of real page
*/
//-----------------------------------------------------------------

void PhysicalMemManager::Print(void) {
  int i;

  printf("Contents of TPR (%d pages)\n",g_cfg->NumPhysPages);
  for (i=0;i<g_cfg->NumPhysPages;i++) {
    printf("Page %d free=%d locked=%d virtpage=%d owner=%lx U=%d M=%d\n",
        i,
        tpr[i].free,
        tpr[i].locked,
        tpr[i].virtualPage,
        (long int)tpr[i].owner,
        (tpr[i].owner!=NULL) ? tpr[i].owner->translationTable->getBitU(tpr[i].virtualPage) : 0,
        (tpr[i].owner!=NULL) ? tpr[i].owner->translationTable->getBitM(tpr[i].virtualPage) : 0);
  }
}
