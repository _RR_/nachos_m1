# TP Nachos: let's understand and fix/upgrade this kernel
Lauric Desauw & Rémi Piau

# TP1: Ordonancement & synchronisation

## Syncronisation tools
We filled the code for each lock in `synch.cc`
Note: we decided to implement semaphores with positive only counters
and a wait list as we found that much simpler and pretty.

We also also filled `exception.cc` which provide syscall handling.
Our code is based on how the threads syscall handling is designed
but we added our own errors and made full check before allowing
some sycall (they throw an error if disallowed).
For example for semaphore destory syscall we check if there are threads still waiting
in P and if this is the case and return a `SYNC_STRUCT_STILL_USED`
without destroying the semaphore.

## Thread Management

We implemented all the requested functions that allows threads to work.
Implementation was relatively smooth sailing when following the instructions.
At this point we can run binaries like `test/hello`.


## Bonus: Time Sharing

In order to make system more realist it has to be doted with time sharing capabilities.
To do that we modfied the machine to have method to enable or disable timer exceptions.
These timer exceptions can now be toggled in kernel.
This is necessary because rescheduling the timer exception make the machine busy
and stop it from halting the program to run is finished.
We can now set a timer handler in kernel that ... TODO

We also adapted nachos configuration file to be able to toggle between
random timer exception and fixed time ones.

## Demo

You can try tp1 demo by running `./demo_tp1.sh`
it will run several programs to show that semaphores, locks and signals are working
and that time sharing rocks/works.




## License
For our work:
Copyright (c) 2020 Lauric Desaw & Rémi Piau
All our work is provided wthout ANY BASIS OR WARRANTY.
Modification and redistribution is allowed for non commercial purpose.

The original files still have the copyright notice on top
