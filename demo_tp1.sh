#!/bin/sh

echo "Welcome to the TP1 Demo"
echo 'By Lauric Desauw & Rémi Piau\n'

echo "First we will build the project"
echo "please update your Makefile.config"
echo "in order to do so (Enter to Continue/ Ctrl-C to Aboard)"
read UNUSED_VAR

echo "Running Make, Please stand by"
sleep 2
make


echo ""
echo "Compile Finished (hopefully)"
echo "Press enter to continue"
read UNUSED_VAR

echo "Running nachos on test/hello"
echo "Press enter to continue"
read UNUSED_VAR
./nachos -x /hello

echo "Running nachos on test/sema1"
echo "That show that threads and semaphores are working"
echo "Press enter to continue"
read UNUSED_VAR
./nachos -x /sema1


echo "Running nachos on test/lock"
echo "That show that threads and locks are working"
echo "Press enter to continue"
read UNUSED_VAR
./nachos -x /lock

echo "Running nachos on test/lock"
echo "That show that threads and locks are working"
echo "Press enter to continue"
read UNUSED_VAR
./nachos -x /cond

echo ""
echo "Hopefully you noticed that time sharing is functionnal"
