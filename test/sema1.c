/* sema1.c
 *	Simple semaphore test
 *
// Copyright (c) 2020, Lauric Desauw & Rémi Piau
// PROVIDED WITHOUT ANY
// BASIS OR WARRANTY
// Free to redistribute on non commercial purpose
*/

#include "userlib/syscall.h"
#include "userlib/libnachos.h"

SemId sema = 0;

void function1() {
  n_printf("We are in the other thread\n");
  n_printf("P on semaphore %d\n", sema);
  P(sema);
  n_printf("We are in the other thread, we are unblocked, finishing\n");
}

int main()
{
  sema = SemCreate("Semaphore1", 0);
  n_printf("Semaphore id: %d\n", sema);

  
  ThreadId thread =  threadCreate("Thread 1", &function1);
  n_printf("Created Thread id: %d\n", thread);
  n_printf("Back in main thread\n");
  n_printf("V on semaphore %d\n", sema);
  V(sema);
  n_printf("After V on semaphore %d\n", sema);
  Join(thread);
  n_printf("Joined with thread %d in main thread\n", thread);
  if(SemDestroy(sema) != 0) {
    n_printf("Destroying failure\n");
  }
  else {
    n_printf("Semaphore destroyed successfully\n");
  }

  return 0;
}
