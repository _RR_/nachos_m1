/* send_tty.c
 *	Test program to use in coordination with receive_tty.c
 *	
 *	Create two thread which try to use the same cond
 // Copyright (c) 2020, Lauric Desauw and Remi Piau
 //  PROVIDED WITHOUT ANY BASIS OR WARRANTY
 // Free to distribute on non commercial purpose
 */

// Nachos system calls
#include "userlib/syscall.h"
#include "userlib/libnachos.h"
#define BUFF_SIZE 100
int main()
{
  char my_string[BUFF_SIZE];
  n_printf("Receiving string...\n");
  if(TtyReceive(my_string, BUFF_SIZE-1) < 0) {
    n_printf("Error\n");
  }
  else {
    n_printf("Received: %s\n", my_string);
  }
  return 0;
}
