/* send_tty.c
 *	Test program to use in coordination with receive_tty.c
 *	
 *	Create two thread which try to use the same cond
 // Copyright (c) 2020, Lauric Desauw and Remi Piau
 //  PROVIDED WITHOUT ANY BASIS OR WARRANTY
 // Free to distribute on non commercial purpose
 */

// Nachos system calls
#include "userlib/syscall.h"
#include "userlib/libnachos.h"

int main()
{
  char* a_string = "Hello there";
  // init send buffer
  n_printf("Sending string: %s\n", a_string);
  TtySend(a_string);
  n_printf("Done\n");
  return 0;
}
