/* cond.c
 *	Simple program to test whether running a user program works.
 *	
 *	Create two thread which try to use the same cond
 // Copyright (c) 2020, Lauric Desauw and Remi Piau
 //  PROVIDED WITHOUT ANY BASIS OR WARRANTY
 // Free to distribute on non commercial purpose
 */

// Nachos system calls
#include "userlib/syscall.h"
#include "userlib/libnachos.h"

CondId id = 0;

void thread1()
{
  n_printf("We are in the secondary thread, we make COndSignal and CondWait\n");
  CondSignal(id);
  CondWait(id);
  return;
}

void thread2()
{
  
  n_printf("We call CondBroadcast\n");
  CondBroadcast(id);
  return;
}

int main()
{
  id = CondCreate("test_cond");
  n_printf("Acquire the cond in the first thread\n");

  n_printf("Create the secondary thread\n");
  ThreadId sec_thread = threadCreate("secondary_thread", &thread1);

  n_printf("We are in the first thread, we make COndSignal and CondWait\n");
  CondSignal(id);
  CondWait(id);

  n_printf("Create the third thread\n");
  ThreadId third_thread = threadCreate("third_thread", &thread2);


  n_printf("We are in the first thread, we make COndSignal and CondWait\n");
  CondSignal(id);
  CondWait(id);
  
  Join(sec_thread);
  Join(third_thread);
  CondDestroy(id);
  return 0;
}
