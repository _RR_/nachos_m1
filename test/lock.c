/* lock.c
 *	Simple program to test whether running a user program works.
 *	
 *	Create two thread which try to use the same lock
 // Copyright (c) 2020, Lauric Desauw and Remi Piau
 //  PROVIDED WITHOUT ANY BASIS OR WARRANTY
 // Free to distribute on non commercial purpose
 */

// Nachos system calls
#include "userlib/syscall.h"
#include "userlib/libnachos.h"

LockId id = 0;

void thread()
{
  n_printf("We'll try to acquire the lock in the secondary thread\n");
  LockAcquire(id);
  n_printf("The secondary thread have acquired the lock\n");
  LockRelease(id);
  return;
}


int main()
{
  id = LockCreate("test_lock");
  n_printf("Acquire the lock in the first thread\n");
  LockAcquire(id);
  n_printf("Call the secondary thread\n");
  ThreadId sec_thread = threadCreate("secondary_thread", &thread);
  Yield();
  n_printf("We've returned from the secondary thread\n");

  n_printf("We release the lock in the first thread\n");
  LockRelease(id);

  Join(sec_thread);
  LockDestroy(id);
  return 0;
}
