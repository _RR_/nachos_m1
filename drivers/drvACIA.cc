/* \file drvACIA.cc
   \brief Routines of the ACIA device driver
//
//      The ACIA is an asynchronous device (requests return 
//      immediately, and an interrupt happens later on).  
//      This is a layer on top of the ACIA.
//      Two working modes are to be implemented in assignment 2:
//      a Busy Waiting mode and an Interrupt mode. The Busy Waiting
//      mode implements a synchronous IO whereas IOs are asynchronous
//      IOs are implemented in the Interrupt mode (see the Nachos
//      roadmap for further details).
//
//  Copyright (c) 1999-2000 INSA de Rennes.
//  All rights reserved.  
//  See copyright_insa.h for copyright notice and limitation 
//  of liability and disclaimer of warranty provisions.
//
*/

/* Includes */

#include "kernel/system.h"         // for the ACIA object
#include "kernel/synch.h"
#include "machine/ACIA.h"
#include "drivers/drvACIA.h"

//-------------------------------------------------------------------------
// DriverACIA::DriverACIA()
/*! Constructor.
  Initialize the ACIA driver. In the ACIA Interrupt mode, 
  initialize the reception index and semaphores and allow 
  reception and emission interrupts. 
  In the ACIA Busy Waiting mode, simply initialize the ACIA 
  working mode and create the semaphore.
  */
//-------------------------------------------------------------------------

DriverACIA::DriverACIA()
{
  if(g_cfg->ACIA == ACIA_BUSY_WAITING) {
    g_machine->acia->SetWorkingMode(BUSY_WAITING);
    receive_sema = new Semaphore((char*)"receive_sema", 1);
    send_sema = new Semaphore((char*)"send_sema", 1);
  }
  if(g_cfg->ACIA == ACIA_INTERRUPT) {
    receive_sema = new Semaphore((char*)"receive_sema", 1);
    send_sema = new Semaphore((char*)"send_sema", 1);
    ind_rec = 0;
    ind_send = 0;
  }
  /*
  printf("**** Warning: contructor of the ACIA driver not implemented yet\n");
  exit(-1);
  */
}

//-------------------------------------------------------------------------
// DriverACIA::TtySend(char* buff)
/*! Routine to send a message through the ACIA (Busy Waiting or Interrupt mode)
*/
//-------------------------------------------------------------------------

int DriverACIA::TtySend(char* buff)
{ 
  if(g_cfg->ACIA == ACIA_BUSY_WAITING) {
    send_sema->P();
    unsigned long long i = 0;
    while(buff[i] != '\0') {
      if(g_machine->acia->GetOutputStateReg() != FULL) {
        g_machine->acia->PutChar(buff[i]);
        i++;
      }
    }
    // wait until not full
    while(g_machine->acia->GetOutputStateReg() == FULL) {}
    g_machine->acia->PutChar('\0');
    send_sema->V();
  }
  if(g_cfg->ACIA == ACIA_INTERRUPT) {
    send_sema->P();
    strncpy(send_buffer, buff, BUFFER_SIZE);
    int working_mode = g_machine->acia->GetWorkingMode();
    g_machine->acia->SetWorkingMode(working_mode | SEND_INTERRUPT);
    g_machine->acia->PutChar(send_buffer[ind_send]);
    ind_send++;
  }
  return 0;
}

//-------------------------------------------------------------------------
// DriverACIA::TtyReceive(char* buff,int length)
/*! Routine to reveive a message through the ACIA 
//  (Busy Waiting and Interrupt mode).
*/
//-------------------------------------------------------------------------

int DriverACIA::TtyReceive(char* buff,unsigned int lg)
{
  if(g_cfg->ACIA == ACIA_BUSY_WAITING) {
    receive_sema->P();
    unsigned long long i = 0;
    while(i < lg) {
      if(g_machine->acia->GetInputStateReg() != EMPTY) {
        buff[i] = g_machine->acia->GetChar();
        if(buff[i] == '\0') {
          break;
        }
        i++;
      }
    }
    receive_sema->V();
  }
  if(g_cfg->ACIA == ACIA_INTERRUPT) {
    receive_sema->P();
    int working_mode = g_machine->acia->GetWorkingMode();
    g_machine->acia->SetWorkingMode(working_mode | REC_INTERRUPT);
    receive_sema->P();
    strncpy(buff, receive_buffer, lg);
    receive_sema->V();
  }
  return 0;
}


//-------------------------------------------------------------------------
// DriverACIA::InterruptSend()
/*! Emission interrupt handler.
  Used in the ACIA Interrupt mode only. 
  Detects when it's the end of the message (if so, releases the send_sema semaphore), else sends the next character according to index ind_send.
  */
//-------------------------------------------------------------------------

void DriverACIA::InterruptSend()
{
  g_machine->acia->PutChar(send_buffer[ind_send]);
  if(ind_send == BUFFER_SIZE-1 || send_buffer[ind_send] == '\0') {
    ind_send = 0;
    g_machine->acia->SetWorkingMode(g_machine->acia->GetWorkingMode()
                                    & ~ SEND_INTERRUPT);
    send_sema->V();
  } else {
    ind_send++;
  }
  /*
  printf("**** Warning: send interrupt handler not implemented yet\n");
  exit(-1);
  */
}

//-------------------------------------------------------------------------
// DriverACIA::Interrupt_receive()
/*! Reception interrupt handler.
  Used in the ACIA Interrupt mode only. Reveices a character through the ACIA. 
  Releases the receive_sema semaphore and disables reception 
  interrupts when the last character of the message is received 
  (character '\0').
  */
//-------------------------------------------------------------------------

void DriverACIA::InterruptReceive()
{
  receive_buffer[ind_rec] = g_machine->acia->GetChar();
  if((ind_rec != 0) && (ind_rec == BUFFER_SIZE-1 || receive_buffer[ind_rec] == '\0')) {
    ind_rec = 0;
    g_machine->acia->SetWorkingMode(g_machine->acia->GetWorkingMode()
                                    & ~ REC_INTERRUPT);
    receive_sema->V();
  }
  else {
    ind_rec++;
  }
  /*
  printf("**** Warning: receive interrupt handler not implemented yet\n");
  exit(-1);
  */
}
